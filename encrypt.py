n = 2754553
e = 7

lowerC = {chr(y+97): y+27 for y in range(26)}
theDict = {chr(y):y-64 for y in range(65,91)}
dictio = {**lowerC, **theDict}
dictio[' ']=53
dictio["'"]=54
dictio["."]=55
dictio["’"]=56

def translate_to_number(string):
    splstr = [char for char in string1]
    getme = list()
    for d in splstr:
        getme.append(dictio[d])
    return getme
    
string1 = input("Type something to test this out: ")
    
appleN = translate_to_number(string1)

def ciphering(number_list,e,n):
    cipher = list()
    for nu in number_list:
        cipher.append(nu**e %n)
    return cipher

cypheredApple = ciphering(appleN,e,n)
print(",".join([str(i) for i in cypheredApple]))