def decrypting(number_list,d,n):
    decry = list()
    for nu in number_list:
        # this takes too much time
        decry.append(int(nu)**d %n)
        print("wait please")
    return decry
    
d = 786063 

n = 2754553

cypheredAppleStr = input("write encoded separated by commas 1,2,3,4: ")

cypheredApple2 = cypheredAppleStr.split(",")
decryptedApple = decrypting(cypheredApple2,d,n)

def translate_to_letter(number_list,dictionary):
    getme = list()
    for d in number_list:
        for key, value in dictionary.items():
            if(value == d):
                getme.append(key)
    getme = "".join(getme)
    return getme
    
lowerC = {chr(y+97): y+27 for y in range(26)}
theDict = {chr(y):y-64 for y in range(65,91)}
dictio = {**lowerC, **theDict}
dictio[' ']=53
dictio["'"]=54
dictio["."]=55
dictio["’"]=56
    
decMes = translate_to_letter(decryptedApple,dictio)
print(decMes)